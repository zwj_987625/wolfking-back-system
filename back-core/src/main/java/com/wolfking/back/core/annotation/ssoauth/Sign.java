package com.wolfking.back.core.annotation.ssoauth;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * 校验登陆注解
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月15日 下午8:19:46
 * @copyright wolfking
 */
@Documented
@Order(Ordered.HIGHEST_PRECEDENCE) // 最高优先级
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface Sign {
	/**
	 * 鉴权码
	 * 
	 * @return
	 */
	String code() default "";
}
