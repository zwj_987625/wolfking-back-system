package com.wolfking.back.core.feignclient;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.Area;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.feign.JerseyFeignConfig;

/**
 * 区域的远程调用feignclient
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月3日下午5:13:57
 * @版权 归wolfking所有
 */
@FeignClient(name = "sso-auth-service", configuration = { JerseyFeignConfig.class })
public interface AreaFeignClient {
	/**
	 * 分页查询区域
	 * 
	 * @param area
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@POST
	@Path("/wolfking/area/page")
	ResponseEntity<PageInfo<Area>> pageArea(@RequestBody(required = true) Area area,
			@HeaderParam("pageNum") int pageNum, @HeaderParam("pageSize") int pageSize);

	/**
	 * 获取所有的区域
	 * 
	 * @return
	 */
	@GET
	@Path("/wolfking/area")
	ResponseEntity<List<Area>> getAllArea();

	/**
	 * 根据ID查询区域
	 * 
	 * @param id
	 * @return
	 */
	@GET
	@Path("/wolfking/area/{id}")
	ResponseEntity<Area> getArea(@PathParam("id") String id);

	/**
	 * 更新区域实体
	 * 
	 * @param area
	 * @return
	 */
	@PUT
	@Path("/wolfking/area")
	ResponseEntity<Boolean> updateArea(@RequestBody(required = true) Area area);

	/**
	 * 添加area
	 * 
	 * @param area
	 * @return
	 */
	@POST
	@Path("/wolfking/area")
	ResponseEntity<Boolean> addArea(@RequestBody(required = true) Area area);

	/**
	 * 删除区域
	 * 
	 * @param id
	 * @return
	 */
	@DELETE
	@Path("/wolfking/area/{id}")
	ResponseEntity<Boolean> deleteArea(@PathParam("id") String id);
}
