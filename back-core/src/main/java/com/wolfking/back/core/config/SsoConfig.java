package com.wolfking.back.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * sso的配置
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月23日 下午5:11:14
 * @copyright wolfking
 */
@ConfigurationProperties(prefix = "sso")
public class SsoConfig {
	// 登录的url
	private String loginUrl = "http://127.0.0.1/login";
	// 是否进行sso的鉴权
	private boolean enabled = true;
	// 本身工程的根url，一般是http://ip:port,没有斜杠
	private String selfRootUrl = "http://127.0.0.1";
	// 切换主题的url
	private String themeUrl = "http://127.0.0.1/theme";
	// 权限异常的url
	private String permissionUrl = "ex";

	/**
	 * @return loginUrl
	 */
	public String getLoginUrl() {
		return loginUrl;
	}

	/**
	 * @param loginUrl
	 *            要设置的 loginUrl
	 */
	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	/**
	 * @return enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled
	 *            要设置的 enabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return selfRootUrl
	 */
	public String getSelfRootUrl() {
		return selfRootUrl;
	}

	/**
	 * @param selfRootUrl
	 *            要设置的 selfRootUrl
	 */
	public void setSelfRootUrl(String selfRootUrl) {
		this.selfRootUrl = selfRootUrl;
	}

	/**
	 * @return themeUrl
	 */
	public String getThemeUrl() {
		return themeUrl;
	}

	/**
	 * @param themeUrl
	 *            要设置的 themeUrl
	 */
	public void setThemeUrl(String themeUrl) {
		this.themeUrl = themeUrl;
	}

	/**
	 * @return permissionUrl
	 */
	public String getPermissionUrl() {
		return permissionUrl;
	}

	/**
	 * @param permissionUrl 要设置的 permissionUrl
	 */
	public void setPermissionUrl(String permissionUrl) {
		this.permissionUrl = permissionUrl;
	}
	
}
