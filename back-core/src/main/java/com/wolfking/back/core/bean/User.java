package com.wolfking.back.core.bean;

import java.util.Date;

import com.wolfking.back.core.annotation.mybatis.MyColumn;
import com.wolfking.back.core.annotation.mybatis.MyTable;

@MyTable("sys_user")
public class User extends DataEntity {

	private static final long serialVersionUID = -3175874324457699554L;
	private String tokenId;
	@MyColumn("company_id")
	private String companyId; // 归属公司
	@MyColumn("office_id")
	private String officeId; // 归属部门
	@MyColumn("login_name")
	private String loginName;// 登录名
	@MyColumn("password")
	private String password;// 密码
	@MyColumn("no")
	private String no; // 工号
	@MyColumn("name")
	private String name; // 姓名
	@MyColumn("email")
	private String email; // 邮箱
	@MyColumn("phone")
	private String phone; // 电话
	@MyColumn("mobile")
	private String mobile; // 手机
	@MyColumn("user_type")
	private String userType;// 用户类型
	@MyColumn("login_ip")
	private String loginIp; // 最后登陆IP
	@MyColumn("login_date")
	private Date loginDate; // 最后登陆日期
	@MyColumn("login_flag")
	private String loginFlag; // 是否允许登陆
	@MyColumn("photo")
	private String photo; // 头像

	/**
	 * @return companyId
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId
	 *            要设置的 companyId
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return officeId
	 */
	public String getOfficeId() {
		return officeId;
	}

	/**
	 * @param officeId
	 *            要设置的 officeId
	 */
	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	/**
	 * @return loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @param loginName
	 *            要设置的 loginName
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            要设置的 password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return no
	 */
	public String getNo() {
		return no;
	}

	/**
	 * @param no
	 *            要设置的 no
	 */
	public void setNo(String no) {
		this.no = no;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            要设置的 name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            要设置的 email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            要设置的 phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            要设置的 mobile
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param userType
	 *            要设置的 userType
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @return loginIp
	 */
	public String getLoginIp() {
		return loginIp;
	}

	/**
	 * @param loginIp
	 *            要设置的 loginIp
	 */
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	/**
	 * @return loginDate
	 */
	public Date getLoginDate() {
		return loginDate;
	}

	/**
	 * @param loginDate
	 *            要设置的 loginDate
	 */
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	/**
	 * @return loginFlag
	 */
	public String getLoginFlag() {
		return loginFlag;
	}

	/**
	 * @param loginFlag
	 *            要设置的 loginFlag
	 */
	public void setLoginFlag(String loginFlag) {
		this.loginFlag = loginFlag;
	}

	/**
	 * @return photo
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * @param photo
	 *            要设置的 photo
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**
	 * @return tokenId
	 */
	public String getTokenId() {
		return tokenId;
	}

	/**
	 * @param tokenId
	 *            要设置的 tokenId
	 */
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
}
