package com.wolfking.back.core.feignclient;

import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;

import com.wolfking.back.core.bean.Menu;
import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.feign.JerseyFeignConfig;

/**
 * 用户sso的远程调用的client
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月23日 下午5:22:34
 * @copyright wolfking
 */
@FeignClient(name = "sso-auth-service", configuration = { JerseyFeignConfig.class })
public interface SsoFeignClient {
	@GET
	@Path("/wolfking/sso/logout{tokenId}")
	ResponseEntity<Boolean> logout(@PathParam("tokenId") String tokenId);

	@GET
	@Path("/wolfking/sso/checklogin/{tokenId}")
	ResponseEntity<User> checkLogin(@PathParam("tokenId") String tokenId);

	@GET
	@Path("/wolfking/sso/{username}/{password}")
	ResponseEntity<User> userLogin(@PathParam("username") String username, @PathParam("password") String password);

	@GET
	@Path("/wolfking/sso/{userId}/authCodes")
	ResponseEntity<Set<String>> getUserAuthCodes(@PathParam("userId") String userId);

	@GET
	@Path("/wolfking/sso/{userId}/menus")
	ResponseEntity<List<Menu>> getUserMenus(@PathParam("userId") String userId);
}
