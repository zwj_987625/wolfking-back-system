package com.wolfking.back.core.feignclient;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.Menu;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.feign.JerseyFeignConfig;

/**
 * 菜单服务的远程调用的client
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月2日上午11:32:09
 * @版权 归wolfking所有
 */
@FeignClient(name = "sso-auth-service", configuration = { JerseyFeignConfig.class })
public interface MenuFeignClient {

	@POST
	@Path("/wolfking/menu/page")
	ResponseEntity<PageInfo<Menu>> pageMenu(@RequestBody(required = true) Menu menu,
			@HeaderParam("pageNum") int pageNum, @HeaderParam("pageSize") int pageSize);

	@GET
	@Path("/wolfking/menu")
	ResponseEntity<List<Menu>> getAllMenu();

	@PUT
	@Path("/wolfking/menu")
	ResponseEntity<Boolean> updateMenu(@RequestBody(required = true) Menu menu);

	@POST
	@Path("/wolfking/menu")
	ResponseEntity<Boolean> addMenu(@RequestBody(required = true) Menu menu);

	@DELETE
	@Path("/wolfking/menu/{id}")
	ResponseEntity<Boolean> deleteMenu(@PathParam("id") String id);

	@GET
	@Path("/wolfking/menu/{id}")
	ResponseEntity<Menu> getMenu(@PathParam("id") String id);
}
