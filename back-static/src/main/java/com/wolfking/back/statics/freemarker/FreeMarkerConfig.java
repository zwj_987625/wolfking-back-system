package com.wolfking.back.statics.freemarker;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * freemarker的自定义标签的配置
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月28日下午3:51:07
 * @版权 归wolfking所有
 */
@Configuration
public class FreeMarkerConfig {

	@Autowired
	private freemarker.template.Configuration configuration;

	@Autowired
	private CustomTags customTags;

	@PostConstruct
	public void setSharedVariable() {
		configuration.setSharedVariable("wolfking", customTags);
	}
}
