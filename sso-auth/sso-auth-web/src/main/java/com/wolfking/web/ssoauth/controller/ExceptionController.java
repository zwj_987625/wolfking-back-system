package com.wolfking.web.ssoauth.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 异常页面
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月14日 下午9:36:18
 * @copyright wolfking
 */
@Controller
public class ExceptionController {

	@RequestMapping("/exception")
	public String exception(Model model, @RequestParam String message, @RequestParam String detail) {
		model.addAttribute("message", message);
		model.addAttribute("detail", detail);
		return "exception";
	}
}
