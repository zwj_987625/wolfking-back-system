package com.wolfking.service.ssoauth.resource;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.exception.ExceptionCode;
import com.wolfking.back.core.util.PasswordUtil;
import com.wolfking.back.core.util.ResponseUtil;
import com.wolfking.service.ssoauth.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Component
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/user", description = "用户的服务", produces = MediaType.APPLICATION_JSON)
public class UserResource {

	@Autowired
	private UserService userService;

	@POST
	@ApiOperation(value = "用户添加接口", httpMethod = "POST", notes = "用户添加接口", response = Boolean.class)
	public Response addUser(@RequestBody User user) {
		return ResponseUtil.okResponse(userService.add(user));
	}

	@PUT
	@ApiOperation(value = "用户修改接口", httpMethod = "PUT", notes = "用户修改接口", response = Boolean.class)
	public Response updateUser(@RequestBody User user) {
		return ResponseUtil.okResponse(userService.update(user));
	}

	@GET
	@ApiOperation(value = "查询所有的用户", httpMethod = "GET", notes = "查询所有的用户", response = User.class, responseContainer = "List")
	public Response getAllUser() {
		return ResponseUtil.okResponse(userService.findAll());
	}

	@GET
	@Path("/{id}")
	@ApiOperation(value = "根据ID查询用户接口", httpMethod = "GET", notes = "根据ID查询用户接口", response = User.class)
	public Response getUser(@PathParam("id") @ApiParam(required = true, name = "id", value = "用户ID") String userId) {
		User user = userService.getById(userId);
		return ResponseUtil.okResponse(user);
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "根据ID删除用户接口", httpMethod = "DELETE", notes = "根据ID删除用户接口", response = Boolean.class)
	public Response deleteUser(@PathParam("id") @ApiParam(required = true, name = "id", value = "用户ID") String userId) {
		return ResponseUtil.okResponse(userService.deleteById(userId));
	}

	@POST
	@Path("/page")
	@ApiOperation(value = "分页查询用户信息", httpMethod = "POST", notes = "分页查询用户信息", response = User.class, responseContainer = "PageInfo")
	public Response pageUser(@RequestBody User user,
			@HeaderParam("pageNum") @ApiParam(name = "pageNum", value = "页码", defaultValue = "1") int pageNum,
			@HeaderParam("pageSize") @ApiParam(name = "pageSize", value = "每页大小", defaultValue = "10") int pageSize) {
		PageInfo<User> pageInfo = userService.pageVague(user, pageNum, pageSize);
		return ResponseUtil.okResponse(pageInfo);
	}

	@GET
	@Path("/modifyPwd/{id}")
	@ApiOperation(value = "用户修改密码", httpMethod = "GET", notes = "用户修改密码", response = Boolean.class)
	public Response modifyPwd(@PathParam("id") String id, @HeaderParam("oldPassword") String oldPassword,
			@HeaderParam("newPassword") String newPassword) {
		User user = userService.getById(id);
		if (!PasswordUtil.validatePassword(oldPassword, user.getPassword()))
			return ResponseUtil.exceptionResponse(ExceptionCode.BIZ_ERROR_USER_COUNT_NAME_NOMATHCH.value(), "原始密码不对");
		user.setPassword(PasswordUtil.entryptPassword(newPassword));
		userService.update(user);
		return ResponseUtil.okResponse(true);
	}

	@POST
	@Path("/seleteAccuracy")
	@ApiOperation(value = "用户的精准查询", httpMethod = "POST", notes = "用户的精准查询", response = User.class, responseContainer = "List")
	public Response seleteAccuracy(@RequestBody User user) {
		List<User> list = userService.seleteAccuracy(user);
		return ResponseUtil.okResponse(list);
	}

	@GET
	@Path("/role/{roleId}")
	@ApiOperation(value = "根据角色查询用户", httpMethod = "GET", notes = "根据角色查询用户", response = User.class, responseContainer = "List")
	public Response getUserListByRoleId(@PathParam("roleId") String roleId) {
		List<User> list = userService.getUserByRoleId(roleId);
		return ResponseUtil.okResponse(list);
	}
}
