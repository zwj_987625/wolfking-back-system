package com.wolfking.service.ssoauth.relation;

import com.wolfking.back.core.annotation.mybatis.MyColumn;
import com.wolfking.back.core.annotation.mybatis.MyTable;

/**
 * 用户和角色的关联关系
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月13日 下午9:19:36
 * @copyright wolfking
 */
@MyTable("sys_user_role")
public class UserRoleRelation {

	@MyColumn("user_id")
	private String userId;

	@MyColumn("role_id")
	private String roleId;

	public UserRoleRelation() {

	}

	public UserRoleRelation(String userId, String roleId) {
		this.roleId = roleId;
		this.userId = userId;
	}

	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return roleId
	 */
	public String getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId
	 *            要设置的 roleId
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

}
