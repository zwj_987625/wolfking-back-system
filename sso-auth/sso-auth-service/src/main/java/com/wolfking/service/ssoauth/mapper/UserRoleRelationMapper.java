package com.wolfking.service.ssoauth.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.wolfking.back.core.mybatis.BaseMapper;
import com.wolfking.service.ssoauth.relation.UserRoleRelation;

@Mapper
public interface UserRoleRelationMapper extends BaseMapper<UserRoleRelation> {

}
