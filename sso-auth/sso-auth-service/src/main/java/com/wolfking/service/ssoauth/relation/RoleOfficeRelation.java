package com.wolfking.service.ssoauth.relation;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.wolfking.back.core.annotation.mybatis.MyColumn;
import com.wolfking.back.core.annotation.mybatis.MyTable;
import com.wolfking.back.core.bean.Role;

import jersey.repackaged.com.google.common.collect.Lists;

/**
 * 角色和菜单的关联关系
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月13日 下午9:19:36
 * @copyright wolfking
 */
@MyTable("sys_role_office")
public class RoleOfficeRelation {

	@MyColumn("role_id")
	private String roleId;
	@MyColumn("office_id")
	private String officeId;

	public RoleOfficeRelation() {

	}

	public RoleOfficeRelation(String roleId, String officeId) {
		this.roleId = roleId;
		this.officeId = officeId;
	}

	/**
	 * @return roleId
	 */
	public String getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId
	 *            要设置的 roleId
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return officeId
	 */
	public String getOfficeId() {
		return officeId;
	}

	/**
	 * @param officeId
	 *            要设置的 officeId
	 */
	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	public static List<RoleOfficeRelation> assemblyRelation(Role role) {
		String roleId = role.getId();
		String officeIds = role.getMenuIds();
		List<RoleOfficeRelation> list = Lists.newArrayList();
		if (StringUtils.isNotEmpty(role.getOfficeIds()))
			for (String officeId : officeIds.split(","))
				if (StringUtils.isNotEmpty(officeId))
					list.add(new RoleOfficeRelation(roleId, officeId));
		return list;
	}
}
