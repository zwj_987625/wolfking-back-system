package com.wolfking.service.ssoauth.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.bean.Role;
import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.util.ResponseUtil;
import com.wolfking.service.ssoauth.service.RoleService;
import com.wolfking.service.ssoauth.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Component
@Path("/role")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/role", description = "角色的服务", produces = MediaType.APPLICATION_JSON)
public class RoleResource {

	@Autowired
	private RoleService roleService;
	@Autowired
	private UserService userService;

	@POST
	@ApiOperation(value = "角色添加接口", httpMethod = "POST", notes = "角色添加接口", response = Boolean.class)
	public Response addRole(@RequestBody Role role) {
		return ResponseUtil.okResponse(roleService.add(role));
	}

	@PUT
	@ApiOperation(value = "角色修改接口", httpMethod = "PUT", notes = "角色修改接口", response = Boolean.class)
	public Response updateRole(@RequestBody Role role) {
		return ResponseUtil.okResponse(roleService.update(role));
	}

	@GET
	@ApiOperation(value = "查询所有的角色", httpMethod = "GET", notes = "查询所有的角色", response = Role.class, responseContainer = "List")
	public Response getAllRole() {
		return ResponseUtil.okResponse(roleService.findAll());
	}

	@GET
	@Path("/{id}")
	@ApiOperation(value = "根据ID查询角色接口", httpMethod = "GET", notes = "根据ID查询角色接口", response = Role.class)
	public Response getRole(@PathParam("id") @ApiParam(required = true, name = "id", value = "用户ID") String id) {
		Role role = roleService.getById(id);
		return ResponseUtil.okResponse(role);
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "根据ID删除角色接口", httpMethod = "DELETE", notes = "根据ID删除角色接口", response = Boolean.class)
	public Response deleteRole(@PathParam("id") @ApiParam(required = true, name = "id", value = "用户ID") String id) {
		return ResponseUtil.okResponse(roleService.deleteById(id));
	}

	@POST
	@Path("/page")
	@ApiOperation(value = "分页查询角色信息", httpMethod = "POST", notes = "分页查询角色信息", response = Role.class, responseContainer = "PageInfo")
	public Response pageRole(@RequestBody Role role,
			@HeaderParam("pageNum") @ApiParam(name = "pageNum", value = "页码", defaultValue = "1") int pageNum,
			@HeaderParam("pageSize") @ApiParam(name = "pageSize", value = "每页大小", defaultValue = "10") int pageSize) {
		PageInfo<Role> pageInfo = roleService.pageVague(role, pageNum, pageSize);
		return ResponseUtil.okResponse(pageInfo);
	}

	@POST
	@Path("/selectAccuracy")
	@ApiOperation(value = "分页查询角色信息", httpMethod = "POST", notes = "分页查询角色信息", response = Role.class, responseContainer = "List")
	public Response selectAccuracy(@RequestBody Role role) {
		return ResponseUtil.okResponse(roleService.seleteAccuracy(role));
	}

	@DELETE
	@Path("/outrole/{roleId}/{userId}")
	@ApiOperation(value = "删除角色和用户的绑定", httpMethod = "DELETE", notes = "删除角色和用户的绑定", response = Boolean.class)
	public Response outrole(@PathParam("roleId") String roleId, @PathParam("userId") String userId) {
		roleService.outrole(roleId, userId);
		return ResponseUtil.okResponse(true);
	}

	@GET
	@Path("/assginrole/{roleId}/{userId}")
	@ApiOperation(value = "角色和用户的绑定", httpMethod = "GET", notes = "角色和用户的绑定", response = User.class)
	public Response assginrole(@PathParam("roleId") String roleId, @PathParam("userId") String userId) {
		roleService.assignrole(roleId, userId);
		User user = userService.getById(userId);
		return ResponseUtil.okResponse(user);
	}

}
