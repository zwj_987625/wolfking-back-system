package com.wolfking.service.ssoauth.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.wolfking.back.core.bean.Menu;
import com.wolfking.back.core.mybatis.BaseMapper;

/**
 * 菜单的mapper
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月27日下午1:54:46
 * @版权 归wolfking所有
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

}
